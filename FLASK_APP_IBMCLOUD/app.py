import os
import json
import requests

from flask import Flask
from flask import Flask, render_template, request, jsonify

from PIL import Image

app = Flask(__name__, static_url_path='/static')

#model = keras.models.load_model('java_detector.h5')

from watson_developer_cloud import VisualRecognitionV3




@app.route('/')
def Welcome():
	return render_template('index.htm')

@app.route('/process_image', methods=['GET', 'POST'])
def process_form_data():
	visual_recognition = VisualRecognitionV3('2016-05-20',api_key='57b410f4bf0bd80491021064800c02ff2ae76421')

	classes = {}
	image_file_from_webcam = request.files['webcam']


	print("TRY CLASSIFY WITH WATSON")
	classes = visual_recognition.classify(image_file_from_webcam,threshold="0.01", classifier_ids="DefaultCustomModel_1636274742")
	return jsonify( classes )
	
port = os.getenv('VCAP_APP_PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
