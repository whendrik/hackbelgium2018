# Hack Belgium 2018 - Example Code

Code and Instruction to create your own Hacker Detector

## 1 - Sign up for IBM Watson Studio, and a IBM Cloud account

https://datascience.ibm.com

## 2 - Create a new project

Within the new project, add a "Visual Recognition Model".
First time you do this, it will route you through the creation of a Watson Image Service.

![im](images/add_visual_model.png)

## 3 - Create a Custom Model, and upload your classes in zip's

Create a new Custom Model within your Visual Recognition Service.

Pack all images (jpg, png) in a zip, and name it to the class, e.g. `programmers.zip`.
The .zip should contain images only, and only from 1 class. Each class gets its own zipfile.

![im](images/classes.png)

## 4 - Edit app.py and template.htm

In the app.py, change `api_key` and `classifier_ids` to correct values of your fresly created service and classifier.

![im](images/code.png) /home/willem/Pictures/classes.png

The class names should also be updated in the `index.htm` within the template folder, for the gauges to update.
You can test the app.py by running `python3 app.py` if you have the required python packages.

## 5 - Install CF tools

https://console.bluemix.net/docs/cli/index.html#overview

## 6 - Deploy - Edit manifest.yml and do a cf push

Edit manifest.yml, to choose a unique name and host. Within the directory of manifest.yml, do;
```
$ cf push
```
your app should run in about 1 minute. 
